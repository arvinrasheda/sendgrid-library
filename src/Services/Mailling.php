<?php

namespace LibMessages;

use Mail;

class Mailling{

    /**
     * Set to
     *
     * @param $to  Mail
     *
     * @return self
     */
    public function to($to)
    {
        $this->to = $to;
        return $this;
    }
    /**
     * Set tittle
     *
     * @param $to  tittle
     *
     * @return self
     */
    public function tittle($tittle)
    {
        $this->tittle = $tittle;
        return $this;
    }
    /**
     * Set frommail
     *
     * @param $from  Mail
     *
     * @return self
     */
    public function frommail($from)
    {
        $this->from = $from;
        return $this;
    }
    /**
     * Set subject
     *
     * @param $subject  Mail
     *
     * @return self
     */
    public function subject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

     /**
     * Set Letter
     *
     * @param $letter  Phone number
     *
     * @return self
     */
    public function letter($letter)
    {
        $this->letter = $letter;
        return $this;
    }
    
    /*
     * Build and Send Mail
     */
    function build(){

        $data = array(
            'name' => "Email Billionaire Store",
        );
        $letter = $this->letter;
    
        Mail::send($letter, $data, function ($message) {
            
            $to = $this->to;
            $subject = $this->subject;
            $from = $this->from;
            $tittle = $this->tittle;

            $message->from($from, $tittle);
            $message->to($to)->subject($subject);
    
        });
    }
}
